# INSTALL LARAVEL IN CURRENT DIRECTORY
1. composer create-project laravel/laravel .

# INSTALL LARAVEL IN DOWNLOADED GIT REPOSITORY
2. composer install
If you want to install on production server add key --no-dev in end of command composer install

# AFTER LARAVEL INSTALL YOU NEED EXECUTE THIS COMMAND
3. sudo chmod 777 -R storage resources bootstrap

# COMMANDS FOR INSTALLATION JVASCRIPT DEPENDENCIES
4. nmp install
5. nmp run dev
