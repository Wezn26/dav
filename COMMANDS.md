# AFTER LARAVEL INSTALL YOU NEED EXECUTE THIS COMMAND
sudo chmod 777 -R storage resources bootstrap
# FOR MIGRATIONS LARAVEL COMMAND
docker-compose exec php-fpm php /var/www/html/artisan migrate
# FOR ROLLBACK MIGRATIONS
docker-compose exec php-fpm php /var/www/html/artisan migrate:rollback
# FOR EXECUTE CLASS SEEDER
docker-compose exec php-fpm php /var/www/html/artisan db:seed --class=UsersSeeder
# FOR REFRESHING MIGRATES SEEDER
docker-compose exec php-fpm php /var/www/html/artisan migrate:fresh --seed
# COMMAND FOR LARAVEL AUTHENTICATION
composer require laravel/ui
php artisan ui:auth
# COMMAND FOR LARAVEL ERROR STORAGE
docker-compose exec php-fpm chmod 777 -R /var/www/html/storage
# NPM
npm install - for installation dependencies
npm run dev - create development structure
